import cv2
from fann2 import libfann as fann 
def entrenamiento(inp,out,capas):
	indice_conexion = 1 #Caracteristica de las conexiones entre capas 1 -> completamente conectada
	rango_aprendizaje = 0.1 #Constante de aprendizaje
	error_deseado = 0.00001
	data = fann.training_data()    
	data.set_train_data(inp,out)
	max_iteraciones = 100000
	ann = fann.neural_net()
	ann.create_sparse_array(indice_conexion, capas)# funcion que crear la red, PARAM indice y vector de neuronas por capa
	ann.set_learning_rate(rango_aprendizaje) #funcion para insertar el rango de aprendizaje,
	ann.set_activation_function_output(fann.SIGMOID) #setear la funcion de activacion 
	#ann.train_on_file("and.data", max_iteraciones, 1000, error_deseado)
	#^Entrenar la red en un archivo PARAM archivo salida, maximo de iteraciones, 
	ann.train_on_data(data,max_iteraciones,1000,error_deseado)
	ann.save("red.net")
inp=[[1,1],[1,0],[0,1],[0,0]]
out=[[1],[0],[0],[0]]
capas=[2,2,1]
entrenamiento(inp,out,capas)